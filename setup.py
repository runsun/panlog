#!/usr/bin/env python

from setuptools import setup

setup(name='panlog-runsun',
      version='0.1.1-20190325',
      description='Tree-like logging util for python debugging',
      author='Runsun Pan',
      author_email='runsun@gmail.com',
      url='https://bitbucket.org/runsun/panlog',
      classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
       ]
     )